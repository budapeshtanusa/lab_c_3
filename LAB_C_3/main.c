/*Определить структурированный тип и набор  функций для работы с таблицей записей, реализованной в массиве структур. В перечень функций входят:
•        ввод записи таблицы с клавиатуры;
•        загрузка и сохранение  таблицы в текстовом файле;
•        просмотр таблицы;
•        сортировка таблицы в порядке возрастания заданного поля;
•        поиск в таблице элемента с заданным значением поля или с наиболее близким к нему по значению;
•        удаление записи;
•        изменение (редактирование) записи;
•        вычисление с проверкой и использованием всех pfgbctq(записей) по заданному условию и формуле (например, общая сумма на всех счетах).
 Перечень полей структурированной переменной:
7. Номер рейса, пункт назначения, время вылета, дата вылета, стоимость билета.*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

struct table {
	int num;
	char dest[30];
	time_t time;
	time_t date;
	int price;

};

void swap_table(struct table *a,struct table *b) {
	int num_a=a->num;
	char dest_a[30];
	memcpy(dest_a,a->dest,sizeof(dest_a));
	time_t time_a=a->time;
	time_t date_a=a->date;
	int price_a=a->price;

	a->num=b->num;
	memcpy(a->dest,b->dest,sizeof(dest_a));
	a->time=b->time;
	a->date=b->date;
	a->price=b->price;

	b->num=num_a;
	memcpy(b->dest,dest_a,sizeof(dest_a));
	b->time=time_a;
	b->date=date_a;
	b->price=price_a;
}




void help(){
	printf("1) Add new note\n");
	printf("2) Save at file\n");
	printf("3) Show table\n");
	printf("4) Sort table\n");
	printf("5) Search\n");
	printf("6) Delete\n");
	printf("7) Change note\n");
	printf("8) Some function\n");
	printf("0) Exit from menu\n");
}

void sort(){
	printf("1) Sort flight numbers\n");
	printf("2) Sort Alfabeth\n");
	printf("3) Sort time\n");
	printf("4) Sort date\n");
	printf("5) Sort price\n");
	printf("0) Exit\n");
}

void change(){
	printf("1) Change flight numbers\n");
	printf("2) Change Destination\n");
	printf("3) Change time\n");
	printf("4) Change date\n");
	printf("5) Change price\n");
	printf("6) Change all\n");
	printf("0) Exit\n");
}

void find(){
	printf("1) Search flight numbers\n");
	printf("2) Search Destination\n");
	printf("3) Search time\n");
	printf("4) Search date\n");
	printf("5) Search price\n");
	printf("0) Exit\n");
}

int main (){
	int num_min;
	time_t time_min;
	time_t date_min;
	int price_min;
	FILE *tt;
	struct table person[100];
	memset(person,0,sizeof(person));
	int m;
	int count=0;
	struct tm tm_t;
	char date_str[15];
	char time_str[10];
	int n;
	int k;
	int sum=0;
	char search_mas[30];
	time_t time_search;
	time_t date_search;

	while (1){
		help();
		scanf("%d",&m);
		switch(m) {
			case 1:
 				memset(&tm_t, 0, sizeof(struct tm));
				printf("Input flight number: ");
				scanf("%d",&person[count].num);
				printf("Input destination: ");
				scanf("%30s",person[count].dest);
				printf("Input time: ");
				scanf("%d:%d",&tm_t.tm_hour,&tm_t.tm_min);
				person[count].time=mktime(&tm_t);
				printf("Input date: ");
 				memset(&tm_t, 0, sizeof(struct tm));
  				scanf("%d.%d.%d", &tm_t.tm_mday, &tm_t.tm_mon, &tm_t.tm_year);
				tm_t.tm_mon-=1;
				tm_t.tm_year-=1900;
				person[count].date=mktime(&tm_t);
				printf("Input price: ");
				scanf("%d",&person[count].price);
				count++;
				break;
			case 2:
			
				tt = fopen ("myfile.txt","w");
				fprintf(tt,"\nFlight number\tDestination\tTime\tDate\t\tPrice\tID");
                                for (int i=0;i<100;i++){
                                        if(person[i].num!=0 && person[i].price!=0){
						strftime(date_str, 15, "%d-%m-%Y", localtime(&person[i].date));
						strftime(time_str, 10, "%H-%M", localtime(&person[i].time));
                                                fprintf(tt,"\n%d\t\t%-15s %s\t%s\t%d\n",person[i].num,person[i].dest,time_str,date_str,person[i].price);
                                        }
                                }
                                fprintf(tt,"-----------------------------------------------------------------\n");
				fclose(tt);
			        break;
			case 3:
				printf("\nFlight number\tDestination\tTime\tDate\t\tPrice\tID");
				for (int i=0;i<100;i++){
					if(person[i].num!=0 && person[i].price!=0){
						strftime(date_str, 15, "%d-%m-%Y", localtime(&person[i].date));
						strftime(time_str, 10, "%H-%M", localtime(&person[i].time));
						printf("\n%d\t\t%-15s %s\t%s\t%d\t%d\n",person[i].num,person[i].dest,time_str,date_str,person[i].price,i);
					}
				}
				printf("-----------------------------------------------------------------\n");
				break;
			case 4:
					sort();
					scanf("%d",&n);
					switch(n){
						case 1:
							for (int i=0;i<count-1;i++){
								for(int j=i+1;j<count;j++){
									if (person[i].num>person[j].num){
										swap_table(&person[i],&person[j]);
									}
								}
							}
							break;
						case 3:
							for (int i=0;i<count-1;i++){
								for(int j=i+1;j<count;j++){
									if (person[i].time>person[j].time){
										swap_table(&person[i],&person[j]);
									}
								}
							}
							break;
						case 4:
							for (int i=0;i<count-1;i++){
								for(int j=i+1;j<count;j++){
									if (person[i].date>person[j].date){
										swap_table(&person[i],&person[j]);
									}
								}
							}
							break;
						case 5:
							for (int i=0;i<count-1;i++){
								for(int j=i+1;j<count;j++){
									if (person[i].price>person[j].price){
										swap_table(&person[i],&person[j]);
									}
								}
							}
							break;
						case 0:
							break;
						case 2:
							for (int i=0;i<count-1;i++){
								for(int j=i+1;j<count;j++){
									if (strcmp(person[i].dest,person[j].dest)>0){
										swap_table(&person[i],&person[j]);
									}
								}
							}
							break;
					}

				break;
			case 5:
				find();
				printf("Where is search: ");
				scanf("%d",&n);
				switch(n){
					case 1:
						printf("Search: ");
						scanf("%d",&k);
						printf("\nFlight number\tDestination\tTime\tDate\t\tPrice\tID\n");
						for(int i=0;i<count;i++){
							if (k-5<=person[i].num && k+5>=person[i].num){
								if(person[i].num!=0 && person[i].price!=0){
									strftime(date_str, 15, "%d-%m-%Y", localtime(&person[i].date));
									strftime(time_str, 10, "%H-%M", localtime(&person[i].time));
									printf("\n%d\t\t%-15s %s\t%s\t%d\t%d\n",person[i].num,person[i].dest,time_str,date_str,person[i].price,i);
								}
							}			
						}
						break;
					case 2:
						printf("Search: ");
						scanf("%30s",search_mas);
						printf("\nFlight number\tDestination\tTime\tDate\t\tPrice\tID\n");
						for(int i=0;i<count;i++){
							if (strcmp(person[i].dest,search_mas)==0){
								strftime(date_str, 15, "%d-%m-%Y", localtime(&person[i].date));
								strftime(time_str, 10, "%H-%M", localtime(&person[i].time));
								printf("\n%d\t\t%-15s %s\t%s\t%d\t%d\n",person[i].num,person[i].dest,time_str,date_str,person[i].price,i);
							}
						}


						break;
					case 3:
						printf("Search: ");
 						memset(&tm_t, 0, sizeof(struct tm));
						scanf("%d:%d",&tm_t.tm_hour,&tm_t.tm_min);
						time_search=mktime(&tm_t);
						printf("\nFlight number\tDestination\tTime\tDate\t\tPrice\tID\n");
						for(int i=0;i<count;i++){
							if(time_search==person[i].time){
								strftime(date_str, 15, "%d-%m-%Y", localtime(&person[i].date));
								strftime(time_str, 10, "%H-%M", localtime(&person[i].time));
								printf("\n%d\t\t%-15s %s\t%s\t%d\t%d\n",person[i].num,person[i].dest,time_str,date_str,person[i].price,i);

							}
						}
						break;
					case 4:
						printf("Search: ");
 						memset(&tm_t, 0, sizeof(struct tm));
  						scanf("%d.%d.%d", &tm_t.tm_mday, &tm_t.tm_mon, &tm_t.tm_year);
						tm_t.tm_mon-=1;
						tm_t.tm_year-=1900;
						date_search=mktime(&tm_t);
						printf("\nFlight number\tDestination\tTime\tDate\t\tPrice\tID\n");
						for(int i=0;i<count;i++){
							if(date_search==person[i].date){
								strftime(date_str, 15, "%d-%m-%Y", localtime(&person[i].date));
								strftime(time_str, 10, "%H-%M", localtime(&person[i].time));
								printf("\n%d\t\t%-15s %s\t%s\t%d\t%d\n",person[i].num,person[i].dest,time_str,date_str,person[i].price,i);
							}
						}
						break;
					case 5:
						printf("Search: ");
						scanf("%d",&k);
						printf("\nFlight number\tDestination\tTime\tDate\t\tPrice\tID\n");
						for(int i=0;i<count;i++){
							if (k-5<=person[i].price && k+5>=person[i].price){
								if(person[i].num!=0 && person[i].price!=0){
									strftime(date_str, 15, "%d-%m-%Y", localtime(&person[i].date));
									strftime(time_str, 10, "%H-%M", localtime(&person[i].time));
									printf("\n%d\t\t%-15s %s\t%s\t%d\t%d\n",person[i].num,person[i].dest,time_str,date_str,person[i].price,i);
								}
							}			
						}

						break;
					case 0:
						break;
				}
				break;
			case 6:
				printf("Input id for delete: ");
				scanf("%d",&n);
				person[n].num=0;
				person[n].price=0;
				break;
			case 7:
				printf("What ID will changed: ");
				scanf("%d",&k);
				change();
				scanf("%d",&n);
				switch(n){
					case 1:
						printf("Input flight number: ");
						scanf("%d",&person[k].num);
						break;
					case 2:
						printf("Input destination: ");
						scanf("%30s",person[k].dest);
						break;
					case 3:
 						memset(&tm_t, 0, sizeof(struct tm));
						printf("Input time: ");
						scanf("%d:%d",&tm_t.tm_hour,&tm_t.tm_min);
						person[k].time=mktime(&tm_t);
						break;
					case 4:
						printf("Input date: ");
 						memset(&tm_t, 0, sizeof(struct tm));
  						scanf("%d.%d.%d", &tm_t.tm_mday, &tm_t.tm_mon, &tm_t.tm_year);
						tm_t.tm_mon-=1;
						tm_t.tm_year-=1900;
						person[k].date=mktime(&tm_t);
						break;
					case 5:
						printf("Input price: ");
						scanf("%d",&person[k].price);
						break;
					case 6:
 						memset(&tm_t, 0, sizeof(struct tm));
						printf("Input flight number: ");
						scanf("%d",&person[k].num);
						printf("Input destination: ");
						scanf("%30s",person[k].dest);
						printf("Input time: ");
						scanf("%d:%d",&tm_t.tm_hour,&tm_t.tm_min);
						person[k].time=mktime(&tm_t);
						printf("Input date: ");
 						memset(&tm_t, 0, sizeof(struct tm));
  						scanf("%d.%d.%d", &tm_t.tm_mday, &tm_t.tm_mon, &tm_t.tm_year);
						tm_t.tm_mon-=1;
						tm_t.tm_year-=1900;
						person[k].date=mktime(&tm_t);
						printf("Input price: ");
						scanf("%d",&person[k].price);
						break;
					case 0:
						break;
				}
				break;
			case 8:
				sum=0;
				for(int i=0;i<count;i++){
					sum+=person[i].price;
				}
				printf("\n\nTotal cost: %d\n\n",sum);
				break;
			case 0:
				return 0;
		}
	}
}




